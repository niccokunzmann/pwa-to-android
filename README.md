## PWA to Android

The aim of this repository is to provide an easy way of publishing open-source PWA's on F-Droid. The metadata for FDroid is still submitted as usual and with a small change, your PWA is distributed on FDroid.

## Motivation

Progressive Web Apps become increasingly popular. They allow a smooth transition of offline and online behavior. They come also with additional risks for users and their freedom as now, websites are allowed to run
without Internet connection (TODO: source scheduling) and still submit usage statistics to analytics servers later (TODO: source one-line google analytics), when a connection to a server can be made. Also, it is common practice to download unreadable, compressed code from servers while the PWA is running.

A huge difference can be made to user-freedom with PWA's. Licenses such as the AGPL give users access to the server code. A free PWA can can retrieve its source code from one location e.g. a GitLab Pages server and provide access to a variety of servers, hosted by a company or individuals by changing the server URL in their settings. An example here would be NextCloud, although not a PWA, allowing self-hosting and asking the the server URL when started.

Policies to guide successful open-source software development are already in place in the F-Droid community. PWA's which are "just a wrapper" for one website without any additional functionality such as offline use or server configuration are rejected as they do not provide additional value. Also, the Non-Free-Net Antifeature marks apps which are communicating with fixed servers may these servers be open-source or not because the user does not have the freedom to cooperatively choose to use the provided or self-hosted servers for their case. The Tracking Antifeature applies to apps which send data to servers without the user's consent (TODO reword Antifeatures, maybe add missing). Since a user's freedom is a strong value in the community, guidance can also be received on how to develop apps which respect freedom.

For open-source PWAs it is mandatory to give access to the source code and good practice to guide through the building process. The source code of the server is not necessarily delivered to the user while the source code of the PWA which runs in the web browser is delivered under an open-source license and as such can be modified and served from other locations than one server. Bigger applications split their web-served user-interface (frontend) from the server side (backend), providing e.g. a JSON API which in turn is used by the frontend to communicate. Following this practice, delivering the frontend of the PWA is possible.

This repository makes it easy for PWAs which can be used stand-alone or are split from their server to be built as an Android app and to be published as an app on F-Droid.

## The HTML/CSS/JavaScript App

To include the PWA in F-Droid, is will need the `metadata` folder describing the app in at least one language (TODO: link to documentation). If you serve your PWA with GitLab pages, the structure of the PWA could look like this:

```
my-app
 +- .gitlab-ci.yml // your deployment scripts
 +- public/        // the code you publish to GitLab Pages
 |  +- index.html
 |  +- icon.png
 |  +- my-app.webmanifest // your PWS manifest file
 +- metadata
    +- en-US              // English description
       +- short_description.txt
       +- full_description.txt
       +- title.txt
```

If you add a metadata file to `fdroid-data`, you can include the following to the make this PWA build:

```yaml
- versionName: '1.0'
    versionCode: 1
    commit: 0000000000000000000000000000000000000000
    modules: # TODO correct
    - $$pwa-to-android@0000000000000000000000000000000000000000$$
    shell: # TODO: correct
    - $$pwa-to-android$$/build.sh --source=./public --destination=./app
    subdir: app
    gradle:
      - yes
```

This will use the default sceleton code for your PWA (TODO: link) and place an Android app around it. It also uses this information:
- the metadata's `title.txt` to provide multilingual launchers
- web manifest for 
  - icons in different resolutions
  - the starter colors
  - how to display - fullscreen or not
- see below for current version information Android's `versionCode` and `versionName`


You can configure the build of the app in the following ways:
- Specify an app sceleton to use
- Override files in the sceleton by placing yours in the corresponding folder.

### Configuration

The build can be configured in the `pwa-to-android.yml` file.

```
skeleton:
  versionCode: 1
  versionName: v1.0
skeleton:
## if specified, uses the skeleton provided there
## You can use git submodules to serve the skeleton or include it in pwa-to-android
  path: path/to/skeleton
## same as path but for building on f-droid
  fdroid:
    path: path/to/$$skeleton$$
```

## Publishing new Versions

Publishing new versions should be as easy as this:
- Edit the `pwa-to-android.yml` to increase `versionCode` and `versionName` (Example: `1.1`).
- Create a `metadata/en-US/changelogs/<versionCode>.txt`
- Create a commit 
    ```
    git add .
    git commit -m"v1.1"
    git push
    git tag v1.1
    git push origin v1.1
    ```

## Gitlab-CI

You can use our docker container or clone and run it.
 
```yaml
build-app:
  image: niccokunzmann/pwa-to-android:latest
  script:
  - pwa-to-android --source=./public --destination=./app
  - gradlew assembleDebug
  artifacts:
    path:
    - ./app/build/output/apk/debug
```

## Creating a Jekyll App on F-Droid

## Add a Node app to F-Droid

## Add a Twine Interactive Story


## The Default Skeleton

The default skeleton has the following features which can be specified in the `pwa-to-android.yml`

```
skeleton:
## how to serve the website
## assets is good for single-file PWAs which do not need localStorage.
## It's like getting the files from the file system directly
#  server: assets

## You can specify localhost and a port will be chosen which remains intact.
## localhost is the default setting.
server: localhost

## The port can be specified
# server: localhost:63940

## You can specify a url and it will be emulated through a proxy.
# server: pwa-to-android.gitlab.io

## Once installed, the app can be switched to from these URLs.
  urls:
  - source: https://example.com/hello.html
    destination: /hello.html
```

## Implementation and Discussion

These are possible ways of implementing the desired content:
- **Own Web View**
  It is possible to wrap apps in an own web view serving the content from one of these:
  - **localhost**
    This adds support for `localStorage`.
  - **assets folder**
    Simple but problematic as links may not open on some apps.
    It is suitible and small code for HTML file.
- [**Apache Cordova**](https://cordova.apache.org/)
  This may change which Android target API and minimum API can be used.
  However, there are a bunch of modules for camera usage, files, and more.
  This may allow people to start with a small PWA, transitioning to Apache COrdova later.
  Also, the Android build is easy to add as it is already added.
  Updates of Apache Cordova can be added easily to new versions of the software.
  TODO: research if this is already used on F-Droid.
- [**Android JS**](https://medium.com/@Chhekur/how-to-build-android-apps-with-node-js-using-android-js-2aa4643be87b)
  TODO: Needs evaluating.
  TODO: research if this is already used on F-Droid.
  


## Research

- Asset links and trusted web activity and pwa https://flaming.codes/posts/trusted-web-activity-create-pwa-android-app
    Google has an easy way of releasing a PWA on the play store. It wuld be great to have something as easy for PWAs on F-Droid.
- iconic and cordova app creation https://docs.microsoft.com/en-us/windows/android/pwa
- F-Droid
  - [Progressive Web Apps](https://forum.f-droid.org/t/progressive-web-apps/1691/3)

